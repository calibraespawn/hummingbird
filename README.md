# Hummingbird Proof of Work

Hummingbird is the name of a derivative of [John Tromp](https://github.com/tromp)'s [Cuckoo Cycle](https://github.com/tromp/cuckoo) that uses 32 bit long adjacency lists for the graph vectors and encodes the solutions as coordinates in a [Hash Chain](https://en.wikipedia.org/wiki/Hash_chain).

## General Description of Algorithm

Hummingbird PoW works by finding unbranched loops in the vector field created by a chain of hashes derived from a seed nonce. Difficulty for the algorithm can be adjusted by changing the required cycle size and the maximum number of elements in the hash chain that can be used in a solution, and this also increases the potential maximum amount of memory required.

The essential nature of the problem domain is that of half-collisions between hashes in a hash chain. The field is a square of 32 bits per side, <sub>2</sub>32<sup>2</sup> permutations, and a roughly gaussian distribution of hashchain elements thanks to the very stochastic results of the Murmur 3 hash.

## ASIC resistant

The only remaining way to prevent specialised hardware being made for mining is essentially to shift the largest cost for hardware to memory. This prevents economy through hardware implementation of computation, that has been the general strategy of ASIC makers up to now.

## GPU resistant

In order to put this algorithm completely out of the reach of GPU miners, the memory requirement has to be greater than the average mid-range motherboard can support. 16 lane PCI express and GPU interconnects allow as much as 4 times the bus speed of AMD cpu front side buses, and nearly 6 times the speed of Intel's fastest Core cpu buses.

So for this reason, the baseline minimum recommended for the difficulty of this algorithm must require at least 20Gb of memory for the search tree.

Potentially specialised compute servers designed to accept more than 3 GPUs could be used but the cost premium negates the benefit and the largest memory available on a single GPU currently is 12Gb and the cards cost more than 30% higher versus the amount of memory they make available.

So this algorithm is specifically designed to perform best on AMD Hypertransport memory buses on systems with at least 32Gb of dual channel DDR4. It will perform quite well on Intel chipsets as well, but AMD's fast FSB will mean AMD is the preferred platform to use.

## Implemented in Golang

The original author of this PoW algorithm is an amateur student for many years of CS and especially the science of machine translation. Golang is one of the 'cleanest' languages in existence, and its low-level nature suits it very well to servers, and potentially even operating systems, as it is not explicitly object oriented, though it provides a clear and simple method to associate functions with data structures, and the possibility of untyped interfaces to abstract as in OOP. It provides a very clean implementation of dynamic arrays and a garbage collector, which thus provides you with the best of OOP, functional programming and static typing...

Golang eliminates many of the potential gotchas caused by programming in relatively low level languages like C, with convenient, useful and clean abstractions for object modelling, without tempting the programmer to overly abstract the implementation.

It also adopts many very sensible conventions, unlike the idiom used in many other languages that can hide all kinds of bugs and inefficiencies.

The author also is very fond of clean, concise logic, and so the implementation should be nearly as optimised as it can be straight out of the box. So long as the author is the primary creative force behind the development of this PoW algorithm, it should not be possible to build a more efficient implementation that lowers memory utilisation to the point that an OpenCL or Cuda version can improve the solution discovery rate.
