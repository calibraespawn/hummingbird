# Hummingbird Proof of Work Reference Solver

It is intended to make as optimal as possible the referencne solver for the Hummingbird Proof of Work. The main avenues of optimisation ideally will only be in more aggressive pruning of the data to reduce the memory footprint, and possibly accelerating the hash function. These options will be minimised as much as possible.

## Data format

### Finite Field Size and Difficulty Parameters

The solutions for Hummingbird consist of a string of adjacency list vectors that are found in the finite field created by an iteration of a hash function on a seed nonce value.

The difficulty for the network consensus contains two parameters - the number of nodes required in a cycle, and the maximum number of hash chain nodes that can be used to find the solution.

The hash size used for Hummingbird will be 64 bits, and the baked in maximum array size as proscribed by the built in golang slice implementation is 32 bits. The range of possible cycle lengths will start at 3, and the actual amount in practice will be determined through progressive analysis of the solution time versus the target block time, and the amount of memory required will dictate the maximum hashchain iteration.

### Scapegoat Binary Search Tree

The reference solver will utilise a Scapegoat Binary Search Tree with a simple allocator that encodes each currently utilised slot in the base 32 bit indexed array in a maximum 512Mb of space as individual bits.

When a value is discarded, its relevant allocator bit will be zeroed and the allocator will give a new slot to the lowest discovered free slot when a new hash chain element is generated and stored into the tree data structure.

The data structure of tree elements consists of 7\*32 bit long references, a pair each of parent/more/less (left and right) and one child element. The total size of each node in the tree is thus 288 bits or 36 bytes. The theoretical maximum, therefore, with 32 bit indices, is 144Gb.

The structure encodes two distinct trees, as the collision searches search for head and tail both in the collisions, and because when one half hits another half of another hashchain element, both must be deleted and added to a simple single 32 bit data storing binary search tree in the branches and rejects stores.
