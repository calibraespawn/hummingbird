// Package haschain
//
package chain

import (
	"github.com/spaolacci/murmur3"
	"gitlab.com/clbr/hummingbird/pkg/util"
)

// Chain - the nonce and all of the chain of hashes used to discover a solution
type Chain struct {
	// Nonce - Initial cryptographically secure random number seed of Chain
	Nonce uint64
	// Last - previous hash in the chain
	Last uint64
	// Length
	Length uint64
}

// New - creates a new Chain
func New(maxIndices uint32) Chain {
	var chain Chain
	chain.Nonce = util.GetRandUint64()
	return chain
}

// Next - generate the next element in the hashchain
func (hc *Chain) Next() uint64 {
	if hc.Last == 0 {
		hc.Length++
		hc.Last = murmur3.Sum64(util.Uint64ToBytes(hc.Nonce))
		return hc.Last
	}
	hc.Length++
	hc.Last = murmur3.Sum64(util.Uint64ToBytes(hc.Last))
	return hc.Last
}
