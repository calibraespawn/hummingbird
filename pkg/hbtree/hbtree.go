// Package hbtree - hummingbird search tree
package hbtree

import (
	"errors"

	"github.com/spaolacci/murmur3"

	"gitlab.com/calibraespawn/hummingbird/pkg/util"
)

// Dtree - dual Tree - stores two search SBSTs used by search and solution. `solref` points to solchain, a 2D array of unsorted cycle ordered solutions.
type Dtree struct {
	parent, more, less [2]uint32
	vector             uint64
	leafdist           [2]uint16
	solref             uint32
}

// Tree - single Tree - A single SBST used by Rejects tree.
type Tree struct {
	parent, more, less uint32
	coord              uint32
	leafdist           uint16
}

// Alloc - data structure for tracking how many nodes exist in the Dtree and enabling them to be deleted for decomposition
type Alloc struct {
	Count uint32
	List  []byte
}

// NewAlloc - creates a new Alloc structure for tracking Dtree node allocations
func NewAlloc() Alloc {
	var list Alloc
	list.List = make([]byte, 1)
	list.New()
	list.New()
	return list
}

// New - Finds the first free slot in the allocation list and marks it. Grows list if it has no free slots
func (a *Alloc) New() (uint32, bool) {
	i, j := uint32(0), uint32(0)
	length := uint32(len(a.List))
	allocated := a.List[i]&(1<<j) != 0
	grew := false
	for i < length && allocated {
		if ^a.List[i] != 0 {
			j++
			allocated = a.List[i]&(1<<j) != 0
			if j > 7 {
				j = 0
				i++
			}
		} else {
			i++
		}
	}
	if j > 7 || i >= uint32(len(a.List)) {
		a.List = append(a.List, 1)
		grew = true
	} else {
		a.List[i] = a.List[i] ^ (1 << j)
	}
	a.Count++
	return i*8 + j, grew
}

// Free - Removes an allocation for a slot in the bitlist of tree node array
func (a *Alloc) Free(slot uint32) error {
	length := uint32(len(a.List))
	remainder := slot % 8
	bytepos := slot / 8
	if bytepos > length {
		return errors.New("Requested slot exceeds allocation list length, invalid request")
	}
	if a.List[bytepos]&1<<remainder == 0 {
		return errors.New("Requested slot is already free")
	}
	a.List[bytepos] ^= 1 << remainder
	a.Count--
	return nil
}

const (
	srchroot = 0
	solroot  = 1
	rejroot  = 0
)

// Root - primary struct for all solver data
type Root struct {
	Nonce      uint64
	Last       uint64
	DtreeStore []Dtree
	DtreeAlloc Alloc
	Rejects    []Tree
	Sols       [][]uint64
}

// NewRoot - creates a new root data structure for solution searches
func NewRoot() Root {
	var root Root
	root.Nonce = util.GetRandUint64()
	root.Last = root.Nonce
	root.DtreeStore = make([]Dtree, 2)
	root.DtreeAlloc = NewAlloc()
	root.Rejects = make([]Tree, 1)
	return root
}

// NewSearchNode - creates a new search node, returns index of new node in DtreeStore
func (r *Root) NewSearchNode(vector uint64) uint32 {
	index, grew := r.DtreeAlloc.New()
	if grew {
		var newNode Dtree
		r.DtreeStore = append(r.DtreeStore, newNode)
	}
	r.DtreeStore[index].vector = vector
	return index
}

// NewHashChainElement - creates a new hashchain element
func (r *Root) NewHashChainElement() uint64 {
	r.Last = murmur3.Sum64(util.Uint64ToBytes(r.Last))
	return r.Last
}

// SearchRejectsFor - looks for a match in the rejects tree, returns true if found
func (r *Root) SearchRejectsFor(coord uint32) (position uint32) {
	finished := false
	position = 0
	// Rejects node 0 is the empty pointer to the tree root, attached to .more
	cursor := r.Rejects[rejroot].more
	if cursor == 0 {
		// for the case tree is empty
		finished = true
	}
	for !finished {
		if r.Rejects[cursor].more == 0 && r.Rejects[cursor].less == 0 {
			// Search hit terminal
			finished = true
		} else if r.Rejects[cursor].coord == coord {
			// found match
			position = cursor
			finished = true
		} else if coord < r.Rejects[cursor].coord {
			// move right (less)
			cursor = r.Rejects[cursor].less
		} else {
			// move left (more)
			cursor = r.Rejects[cursor].more
		}
	}
	return position
}

// AddReject - adds a coord to rejects tree
func (r *Root) AddReject(coord uint32) (err error) {
	finished := false
	err = nil
	// start from current root node
	cursor := r.Rejects[rejroot].more
	// if tree is empty, add as root
	if cursor == 0 {
		var newNode Tree
		r.Rejects = append(r.Rejects, newNode)
		// point rejects root pointer to node 1
		r.Rejects[rejroot].more = 1
		r.Rejects[1].coord = coord
		r.Rejects[1].leafdist = 1
		finished = true
	}
	for !finished {
		if r.Rejects[cursor].more == 0 && r.Rejects[cursor].less == 0 {
			// found terminal where to insert
			var newNode Tree
			newNode.coord = coord
			newNode.parent = cursor
			newIndex := uint32(len(r.Rejects))
			r.Rejects = append(r.Rejects, newNode)
			if coord > r.Rejects[cursor].coord {
				// join new element to tree on left (more)
				r.Rejects[cursor].more = newIndex
				r.Rejects[newIndex].parent = cursor
				r.Rejects[newIndex].leafdist = r.Rejects[cursor].leafdist + 1
				// update leafdist back to root
				for cursor != rejroot {
					r.Rejects[cursor].leafdist = r.Rejects[newIndex].leafdist
					cursor = r.Rejects[cursor].parent
				}
			} else {
				// join new element to tree on right (less)
				r.Rejects[cursor].less = newIndex
				r.Rejects[newIndex].parent = cursor
				r.Rejects[newIndex].leafdist = r.Rejects[cursor].leafdist + 1
				// update leafdist back to root
				for cursor != rejroot {
					r.Rejects[cursor].leafdist = r.Rejects[newIndex].leafdist
					cursor = r.Rejects[cursor].parent
				}
			}
		} else if coord == r.Rejects[cursor].coord {
			// match found, no need to insert
			finished = true
			err = errors.New("coord is already in rejects tree")
		} else if coord < r.Rejects[cursor].coord {
			cursor = r.Rejects[cursor].less
		} else if coord > r.Rejects[cursor].coord {
			cursor = r.Rejects[cursor].more
		}
	}
	return err
}

// Insert - searches for the correct position in the search data structure for a new hashchain vector
func (r *Root) Insert(vector uint64) error {

	return nil
}
