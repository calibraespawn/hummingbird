package hbtree

import (
	"log"
	"testing"
)

func TestAlloc(t *testing.T) {
	list := NewAlloc()
	if list.Count != 2 || list.List[0] != 3 {
		t.Error("Failed to correctly create allocation list")
	}
	for i := 0; i < 17; i++ {
		list.New()
	}
	if list.Count != 19 ||
		list.List[0] != 255 ||
		list.List[1] != 255 ||
		list.List[2] != 7 {
		t.Error("failed to generate new elements")
	}
	list.Free(5)
	if list.Count != 18 || list.List[0] != 223 {
		t.Error("did not correctly deallocate elements")
	}
	list.Free(9)
	if list.Count != 17 || list.List[0] != 223 ||
		list.List[1] != 253 {
		t.Error("did not correctly deallocate elements")
	}
	list.New()
	if list.Count != 18 || list.List[0] != 255 ||
		list.List[1] != 253 {
		t.Error("did not correctly allocate elements with holes")
	}
	list.New()
	if list.Count != 19 || list.List[0] != 255 ||
		list.List[1] != 255 {
		t.Error("did not correctly allocate elements with holes")
	}
	log.Println(list)

}
