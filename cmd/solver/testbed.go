package main

import (
	"log"

	"gitlab.com/calibraespawn/hummingbird/pkg/hbtree"
)

func main() {
	newRoot := hbtree.NewRoot()
	log.Println("tree store", newRoot.DtreeStore)
	log.Println("tree alloc list", newRoot.DtreeAlloc)
	log.Println("rejects", newRoot.Rejects)
	log.Println("sols", newRoot.Sols)
	log.Println(newRoot.NewSearchNode())
	log.Println("tree store", newRoot.DtreeStore)
	log.Println("tree alloc list", newRoot.DtreeAlloc)
}
